\section{Introduction}

Content recommendation is the task of directing users' attention to those items that match their personal interests among a large number of different options. Recommendation systems have successfully applied for suggesting text documents such as books \cite{mooney2000content}, news articles \cite{billsus1999personal} and web sites \cite{pazzani1996syskill}. In these sources, users are passive consumers of lengthy texts or  they can barely interact by posting comments or blogs. With the explosive growth of micro-blogging services in the past years, recommendation systems for such platforms have also been extensively studied \cite{abel2011analyzing,chen2010short,hannon2010recommending,weng2010twitterrank,zhou2014real}. 

Unlike the traditional domains, micro-blogging services focus on the instant communication and interaction of users. Users update their posts, often carelessly and ungrammatically, in real-time through any electronic device like their mobile phones and explicitly connect with each other to disseminate and consume information. Although messages are restricted to be rather short, within this short length users have become accustomed to enclose a wealth of information by using techniques such as abbreviations and URL shortening. We focus our work on recommendations on Twitter since it is the most popular micro-blogging service with over 300 million monthly active users as of 2016 \footnote{http://www.statista.com/statistics/282087/number-of-monthly-active-twitter-users/recorded on 21 of August 2016}.    

Messages on Twitter are called \textit{tweets} or \textit{statuses} and users choose to receive statuses' updates of other user accounts by \textit{following} them. They can also re-tweet their followees’ statuses to share information with followers. The number of messages everyday transmitted on Twitter has jumped from 35 million tweets per day in 2010 to over 500 million in 2016 \footnote{http://www.internetlivestats.com/twitter-statistics/ recorded on 21 of August 2016}, leading to users being overwhelmed with information. Nonetheless, Twitter has set up only a few inadequate tweets filtering mechanisms. 

For example, users track the updates only of those people they follow but the statuses are presented in chronological order as the newness of a tweet is considered more significant than its relatedness with the user's interests. In order for a user to reach those posts that truly cater for his concerns, he may have to skip several statuses, a situation which is rather intractable. Additionally, Twitter offers a list of the trending topics all over the world or the possibility to attend regional news. However, the same tweets are presented to all users irrespective of personal interest. From the above insufficiencies of Twitter itself, the need for personalized recommendation of Twitter content stems; to this end several methods that utilize either internal Twitter data or external information have been proposed from the research community \cite{chen2010short,duan2010empirical,godin2013using,hannon2010recommending,weng2010twitterrank}.
In our study we thoroughly examine a bulk of strategies from literature for ranking the tweets of a user's timeline. We exclusively consider content-based information found on tweets' textual content, disregarding any exogenous resource like web pages accessed by tweets' URLs or Semantic Web ontologies.(is justification needed here?)
 
The base method we use is topic relevance used in several papers \cite{chen2010short,balabanovic1997fab,chen2009make,mooney2000content} which consists of the following steps: A model is built both for documents by extracting a set of features from them and for the user to represent the user's preferences, by combining models of documents he has interacted with before. Then, for matching the user model against candidate documents' models, a similarity metric is most often used and a ranking list is returned with those documents with the highest similarity scores placed on the top. At the core of the above process lies the model of the document which determines the attributes that covert an unstructured text into a representation revealing the document characteristics. 

The most popular text-based models in the literature are the bag models which come in two variants: the token n-grams and the character n-grams models. Both models assign a vector to each document or user, but the former considers individual words or sequences of adjacent words as dimensions while the latter considers sequences of adjacent characters. The bag-of-tokens model is popular in recommendation systems \cite{balabanovic1997fab,billsus1999personal,mooney2000content,pazzani1996syskill} whereas the bag-of-characters is mostly applied to domains such as text classification and sentiment analysis( reference needed;). The determinant factor for the performance of the above models is the value of n which defines whether the model makes the bag-of-words assumption (for n=1), or preserves the relative ordering between word or character tokens (for n $>$ 1). 

Despite their popularity, the bag models suffer from high dimensional feature spaces whose size increases with the increase of n. Also, they cannot distinguish between different semantic meanings of the same word without techniques such as Lemmatization which are yet language-dependent and not applicable to multi-lingual environments like microblogs. Topic models  \cite{blei2012probabilistic,steyvers2007probabilistic} ameliorate these problems by modeling textual content into a topic-space of fixed low dimensionality and by capturing polysemy through uncertainty over topics \cite{steyvers2007probabilistic}. In essence, they discover latent topics in a collection of documents by counting word co-occurrence patterns. Documents can be represented as a distribution over the uncovered topics.  They have been effectively utilized for lengthy texts like news articles and papers' abstracts. On the contrary, micro-blogging short messages like tweets challenge the application of topic models due to their noisy nature and the scarcity of word co-occurrence patterns. For this reason, aggregation strategies have been employed to form lengthy pseudo-documents by merging tweets that adhere to some commonalities.In our experiments, we test several configurations of topic model' internal parameters as well as we examine different aggregation strategies for tweets. 

Another text model effectively used for text summarization and text classification is the n-gram graphs which represents documents as undirected graphs \cite{giannakopoulos2008summarization}. This is a language-agnostic model that allocates one node per token or character n-gram and it goes beyond the bag-of-words assumption by connecting two nodes with one weighted edge denoting the frequency of co-occurrence of the corresponding n-grams. In that way it adds rich contextual information to the model and copes with spelling and grammatical mistakes. (polysemy? graph models help and why?). 

Subsequent to the above discussion, this work answers the following four questions: (1) Which is the best configuration of each representation model? (2) How robust is the performance of every representation model in terms of effectiveness? (3) Which is the most effective model for recommending short texts in micro-blogging systems? (4) Which information source for building user models is the best one with respect to effectiveness?
% - ποια αναπαράσταση είναι καλή για recommendation;
% - είναι οι αναπαραστάσεις ευαίσθητες σε αλλαγή παραμέτρων;
% - πόσο καλά δουλεύουν τα μοντέλα cross-lingually;
% - είναι καλά τα topic models για online recommendation settings;

  
In this paper, we start with introducing a taxonomy of the text models mentioned above which relies on the caution about n-gram ordering shown by the models. In this way, we gain a deeper insight into each model endogenous characteristics. Thus, we can better interpret each model performance and we can estimate the impact of the n-gram order preserving factor. Then, we thoroughly evaluate X representation models by conducting experiments on a Twitter dataset. We also exhaustively examine a large number of configurations for each model; in total, we test X configurations for the bag, Y for graph and K for the topic models.
\textbf{Contributions.} Our contributions are summarized as follows:
\begin{itemize}
\item We perform the first systematic study that considers X different representation models, involving both n-gram graphs, bag models and Y state-of-the-art topic models. The code along with the dataset used for our experiments are publicly available.
\item We introduce a taxonomy to elucidate our experimental results. To the best of our knowledge there is no other equally comprehensive categorization in the literature including both n-gram graphs, bag and topic models. We demonstrate that each category yields different results, with the fully order-preserving models outperforming the others.
\item We determine the best configuration for each representation model, the most effective model and the most robust one across all configurations. In that way, we direct researchers to select the most effective or robust representation strategy or to fine-tune a model they already use for recommendations, not only on Twitter, but also on other micro-blogging services with similar characteristics.
\item ...άλλο...
\end{itemize}


%\textbf{List of useful URLs:}
%https://www.quora.com/What-are-some-good-papers-about-topic-modeling-on-Tweets

%https://github.com/datquocnguyen

%http://dbpubs.stanford.edu:8091/~klein/javadoc/edu/stanford/nlp/cluster/AbstractClusteringMethod.html

%https://sourceforge.net/p/textmodels/code/HEAD/tree/trunk/RepresentationModels/

%https://sourceforge.net/p/text-analysis/svn/HEAD/tree/text-analysis/trunk/

%http://alias-i.com/lingpipe/docs/api/index.html

%http://mallet.cs.umass.edu/

%http://jgibblda.sourceforge.net

% \textbf{Σημειώσεις:}
% Ξεκινώντας με τα πειράματα, προτείνω κατ'αρχήν να προσθέσουμε το default
% chronological model του Twitter σε αυτά που θα εξετάσουμε. Το να παρουσιάζουμε
% δηλαδή στο χρήστη τα tweets με τη χρονολογική σειρά με την οποία δημοσιεύονται,
% χωρίς κάποιο άλλο processing. Έτσι, έχουμε ένα μπούσουλα του πόσο σημαντικό
% είναι το content-based ranking από μόνο του. Επίσης, δεν είμαι σίγουρος αν τα
% token- και character-based models πρέπει να θεωρηθούν διαφορετικά μοντέλα ή σαν
% μια configuration επιλογή των bag και graph models. Τι λέτε; Μπορούμε να
% επιχειρηματολογήσουμε υπέρ της ανεξάρτητης θεώρησής τους;
% 
% Για το configuration των token-based graph models, θεωρώ ότι αρκεί να εξετάσουμε
% n=\{1,2,3\} σε συνδυασμό με containment, value και normalized value similarities.
% Συνολικά, 9 configurations.
% 
% Για το configuration των character-based graph models, θεωρώ ότι αρκεί να
% εξετάσουμε n=\{2,3,4\} σε συνδυασμό με containment, value και normalized value
% similarities. Συνολικά, 9 configurations.
% 
% Για το configuration των token-based bag models, θεωρώ ότι αρκεί να εξετάσουμε
% n=\{1,2,3\} με boolean, frequency και tf-idf weights για τα vectors σε συνδυασμό
% με cosine και Jaccard similarity. Το tf-idf δεν έχει νόημα με το Jaccard
% similarity, οπότε έχουμε συνολικά 15 configurations. Υπάρχει κάποιο άλλο
% similarity που πρέπει να προσθέσουμε; Επίσης, θεωρώ ότι δεν πρέπει να κάνουμε
% feature selection, αλλά να κρατάμε όλα τα tokens, είτε είναι stop-words είτε
% είναι πάρα πολύ infrequent. Τέλος, δεν χρειάζεται να κάνουμε stemming ή
% lemmatization με δεδομένο ότι έχουμε multilingual content, χωρίς να ξέρουμε σε
% ποια γλώσσα ακριβώς είναι γραμμένο το κάθε tweet.
% 
% Για το configuration των character-based bag models, ισχύουν τα ίδια με τα
% token-based bag models, με τη διαφορά ότι εξετάζουμε n=\{2,3,4\}.
% 
% Για τα υπόλοιπα μοντέλα, το configuration νομίζω εξαρτάται από την υλοποίηση που
% θα χρησιμοποιήσουμε.
% 
% Για τα πειράματα με τα static models των επιλεγμένων χρήστων, μπορούμε να
% ακολουθήσουμε το setup του
% $http://wume.cse.lehigh.edu/~lih307/reading_group/sigir2012.pdf$. Δηλαδή, να
% πάρουμε τα πρώτα 4/5 των tweets σαν training set και τα υπόλοιπα σαν testing
% set. Τα tweets του testing set παρουσιάζονται στη συνέχεια στον χρήστη σε
% batches των 5 tweets, από τα οποία το 1 είναι positive sample (retweet) και τα
% υπόλοιπα negative samples (non-retweets). Λέτε να αυξήσουμε το μέγεθος σε 10
% tweets, βάζοντας περισσότερα negative samples; Στην περίπτωση των dynamic
% models, απλά το training set χτίζεται on-the-fly.
% 
% Σαν effectiveness measures, μπορούμε να έχουμε είτε το Mean Average Precision
% του $http://wume.cse.lehigh.edu/~lih307/reading_group/sigir2012.pdf$ είτε το Mean
% reciprocal rank.
% 
% Σαν efficiency measure, θα έχουμε το mean overhead time για το ranking των
% batches με τα 5 tweets.
% 
% Επίσης, θα κερδίσουμε πολλούς πόντους αν πούμε ότι διαθέτουμε σε open-source τον
% κώδικα και τα δεδομένα των πειραμάτων.